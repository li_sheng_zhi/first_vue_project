﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp4000.Domain
{
    public class DbinitializeHelper
    {
        public static void initilizer()
        {
            using (var db = new Admin4000Context())
            {
               var dbExist= db.Database.EnsureCreated();
                var tempBrand = db.Brands.Any();
                if (!tempBrand)
                {
                    var brand = new Brand
                    {
                        BrandName = "huawei",
                        Description = "这是一个品牌"
                    };
                    db.Brands.Add(brand);

                    db.SaveChanges();

                    db.Products.AddRange(new Product[]

                    {
                    new Product
                    {
                        ProductName="Huawei nove 3",
                        ShortDesc=".0..",
                        FullDesc="0.0.0",
                        BranId=brand.Id
                    },
                    new Product
                    {
                        ProductName="Huawei",
                        ShortDesc="123456",
                        FullDesc="123456",
                        BranId=brand.Id
                    },
                    }
                        );
                }
               
                db.SaveChanges();
            }
        }
    }
}
