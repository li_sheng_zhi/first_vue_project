﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp4000.Domain
{
    public abstract class BaseEntity
    {
        public BaseEntity()
         {
            IsActived= true;
            IsDeleted=true;
            CreatedTime=DateTime.Now;
            UpdatedTime=DateTime.Now;
         }
        
        public int Id { get; set; }

        public bool IsActived { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }
        
        public string Remarks { get; set; }
    }
}
