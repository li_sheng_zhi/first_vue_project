﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp4000.Domain
{
    public class Product:BaseEntity
    {
        public string ProductName { get; set; }

        public string ShortDesc { get; set; }

        public string FullDesc { get; set; }

        public int BranId { get; set; }
        
        public Brand Brand { get; set; }
    }
}
