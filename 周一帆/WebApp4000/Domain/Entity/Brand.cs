﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp4000.Domain
{
    public class Brand:BaseEntity
    {
        public string BrandName { get; set; }

        public string BrandId { get; set; }

        public string Description { get; set; }

        public IEnumerable<Product> Products { get; set; }
    }
}
